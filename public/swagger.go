package swagger

import (
	rep "gitlab.com/nichego.takogo.azaza/go-library/repository"
)

//go:generate swagger generate spec -o swagger.json --scan-models

// swagger:route POST /createauthor author createaRequest
// responses:
//   200:createaResponse
//	 500:createa500Response

// swagger:parameters createaRequest
type createaRequest struct {
	// New author's info
	// in:body
	// required:true
	Author rep.NewAuthor `json:"author"`
}

// swagger:response createaResponse
type createaResponse struct{}

// swagger:response createa500Response
type createa500Response struct{}

// swagger:route POST /readauthor author readaRequest
// responses:
//   200:readaResponse
//	 400:reada400Response

// swagger:parameters readaRequest
type readaRequest struct {
	// Author's id
	// in:formData
	// required:true
	ID uint `json:"id"`
}

// swagger:response readaResponse
type readaResponse struct {
	// in:body
	Body rep.Author
}

// swagger:response reada400Response
type reada400Response struct{}

// swagger:route PUT /updateauthor author updateaRequest
// responses:
//   200:updateaResponse
//	 400:updatea400Response

// swagger:parameters updateaRequest
type updateaRequest struct {
	// Author's new data
	// in:body
	// required:true
	Author rep.Author `json:"author"`
}

// swagger:response updateaResponse
type updateaResponse struct{}

// swagger:response updatea400Response
type updatea400Response struct{}

// swagger:route DELETE /deleteauthor author deleteaRequest
// responses:
//   200:deleteaResponse
//	 400:deletea400Response

// swagger:parameters deleteaRequest
type deleteaRequest struct {
	// Author's id
	// in:formData
	// required:true
	ID uint `json:"id"`
}

// swagger:response deleteaResponse
type deleteaResponse struct{}

// swagger:response deletea400Response
type deletea400Response struct{}

// swagger:route GET /listauthor author listaRequest
// responses:
//   200:listaResponse
//	 500:lista500Response

// swagger:parameters listaRequest
type listaRequest struct{}

// swagger:response listaResponse
type listaResponse struct {
	// in:body
	Body []rep.Author
}

// swagger:response lista500Response
type lista500Response struct{}

// swagger:route GET /top author topRequest
// responses:
//   200:topResponse
//	 500:top500Response

// swagger:parameters topRequest
type topRequest struct{}

// swagger:response topResponse
type topResponse struct {
	// in:body
	Body []rep.Author
}

// swagger:response top500Response
type top500Response struct{}

// swagger:route POST /createbook book createbRequest
// responses:
//   200:createbResponse
//	 500:createb500Response

// swagger:parameters createbRequest
type createbRequest struct {
	// New book's info
	// in:body
	// required:true
	Book rep.NewBook `json:"book"`
}

// swagger:response createbResponse
type createbResponse struct{}

// swagger:response createb500Response
type createb500Response struct{}

// swagger:route POST /readbook book readbRequest
// responses:
//   200:readbResponse
//	 400:readb400Response

// swagger:parameters readbRequest
type readbRequest struct {
	// Book's id
	// in:formData
	// required:true
	ID uint `json:"id"`
}

// swagger:response readbResponse
type readbResponse struct {
	// in:body
	Body rep.Book
}

// swagger:response readb400Response
type readb400Response struct{}

// swagger:route PUT /updatebook book updatebRequest
// responses:
//   200:updatebResponse
//	 400:updateb400Response

// swagger:parameters updatebRequest
type updatebRequest struct {
	// Book's new data
	// in:body
	// required:true
	Book rep.Book `json:"book"`
}

// swagger:response updatebResponse
type updatebResponse struct{}

// swagger:response updateb400Response
type updateb400Response struct{}

// swagger:route DELETE /deletebook book deletebRequest
// responses:
//   200:deletebResponse
//	 400:deleteb400Response

// swagger:parameters deletebRequest
type deletebRequest struct {
	// Book's id
	// in:formData
	// required:true
	ID uint `json:"id"`
}

// swagger:response deletebResponse
type deletebResponse struct{}

// swagger:response deleteb400Response
type deleteb400Response struct{}

// swagger:route GET /listbook book listbRequest
// responses:
//   200:listbResponse
//	 500:listb500Response

// swagger:parameters listbRequest
type listbRequest struct{}

// swagger:response listbResponse
type listbResponse struct {
	// in:body
	Body []rep.Book
}

// swagger:response listb500Response
type listb500Response struct{}

// swagger:route POST /createuser user createuRequest
// responses:
//   200:createuResponse
//	 500:createu500Response

// swagger:parameters createuRequest
type createuRequest struct {
	// New user's info
	// in:body
	// required:true
	User rep.NewUser `json:"user"`
}

// swagger:response createuResponse
type createuResponse struct{}

// swagger:response createu500Response
type createu500Response struct{}

// swagger:route POST /readuser user readuRequest
// responses:
//   200:readuResponse
//	 400:readu400Response

// swagger:parameters readuRequest
type readuRequest struct {
	// User's id
	// in:formData
	// required:true
	ID uint `json:"id"`
}

// swagger:response readuResponse
type readuResponse struct {
	// in:body
	Body rep.User
}

// swagger:response readu400Response
type readu400Response struct{}

// swagger:route PUT /updateuser user updateuRequest
// responses:
//   200:updateuResponse
//	 400:updateu400Response

// swagger:parameters updateuRequest
type updateuRequest struct {
	// User's new data
	// in:body
	// required:true
	User rep.User `json:"user"`
}

// swagger:response updateuResponse
type updateuResponse struct{}

// swagger:response updateu400Response
type updateu400Response struct{}

// swagger:route DELETE /deleteuser user deleteuRequest
// responses:
//   200:deleteuResponse
//	 400:deleteu400Response

// swagger:parameters deleteuRequest
type deleteuRequest struct {
	// User's id
	// in:formData
	// required:true
	ID uint `json:"id"`
}

// swagger:response deleteuResponse
type deleteuResponse struct{}

// swagger:response deleteu400Response
type deleteu400Response struct{}

// swagger:route GET /listuser user listuRequest
// responses:
//   200:listuResponse
//	 500:listu500Response

// swagger:parameters listuRequest
type listuRequest struct{}

// swagger:response listuResponse
type listuResponse struct {
	// in:body
	Body []rep.User
}

// swagger:response listu500Response
type listu500Response struct{}

// swagger:route POST /bookrental user rentRequest
// responses:
//   200:rentResponse
//	 400:rent400Response

// swagger:parameters rentRequest
type rentRequest struct {
	// User's and Book's id
	// in:body
	// required:true
	RentBook rep.RentBook `json:"rentbook"`
}

// swagger:response rentResponse
type rentResponse struct{}

// swagger:response rent400Response
type rent400Response struct{}

// swagger:route POST /bookreturn user returnRequest
// responses:
//   200:returnResponse
//	 400:return400Response

// swagger:parameters returnRequest
type returnRequest struct {
	// User's and Book's id
	// in:body
	// required:true
	RentBook rep.RentBook `json:"rentbook"`
}

// swagger:response returnResponse
type returnResponse struct{}

// swagger:response return400Response
type return400Response struct{}
