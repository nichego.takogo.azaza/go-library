package main

import (
	"fmt"

	"gitlab.com/nichego.takogo.azaza/go-library/fakeit"
	"gitlab.com/nichego.takogo.azaza/go-library/web"
)

func main() {
	err := fakeit.IfDBEmpty()
	if err != nil {
		fmt.Println(err)
	}
	web.StartServer(":8080")
}
