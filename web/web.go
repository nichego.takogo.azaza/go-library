package web

import (
	"context"
	"encoding/json"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	rep "gitlab.com/nichego.takogo.azaza/go-library/repository"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, nil)
	if err != nil {
		return
	}
}

var DB = rep.NewDB()

func StartServer(port string) {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Post("/createauthor", CreateAuthor)
	r.Post("/readauthor", ReadAuthor)
	r.Put("/updateauthor", UpdateAuthor)
	r.Delete("/deleteauthor", DeleteAuthor)
	r.Get("/listauthor", ListAuthor)
	r.Post("/createbook", CreateBook)
	r.Post("/readbook", ReadBook)
	r.Put("/updatebook", UpdateBook)
	r.Delete("/deletebook", DeleteBook)
	r.Get("/listbook", ListBook)
	r.Post("/createuser", CreateUser)
	r.Post("/readuser", ReadUser)
	r.Put("/updateuser", UpdateUser)
	r.Delete("/deleteuser", DeleteUser)
	r.Get("/listuser", ListUser)
	r.Post("/bookrental", BookRental)
	r.Post("/bookreturn", BookReturn)
	r.Get("/top", Top)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public", http.FileServer(http.Dir("/app/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}
	// Запуск веб-сервера в отдельной горутине
	go func() {
		log.Printf("server started on port %s\n", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

func CreateAuthor(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	var n rep.NewAuthor
	err := json.Unmarshal([]byte(query), &n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = DB.CreateAuthor(n.Name, n.Experience)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func ReadAuthor(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	id, err := strconv.ParseUint(query, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	author, err := DB.ReadAuthor(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	js, err := json.Marshal(author)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(js)
}
func UpdateAuthor(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	err := DB.UpdateAuthor([]byte(query))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func DeleteAuthor(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	id, err := strconv.ParseUint(query, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = DB.DeleteAuthor(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func ListAuthor(w http.ResponseWriter, r *http.Request) {
	authors, err := DB.ListAuthor()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(authors)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(js)
}
func Top(w http.ResponseWriter, r *http.Request) {
	users, err := DB.Top()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(js)
}

func CreateBook(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	var n rep.NewBook
	err := json.Unmarshal([]byte(query), &n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = DB.CreateBook(n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func ReadBook(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	id, err := strconv.ParseUint(query, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	book, err := DB.ReadBook(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	js, err := json.Marshal(book)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(js)
}
func UpdateBook(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	err := DB.UpdateBook([]byte(query))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func DeleteBook(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	id, err := strconv.ParseUint(query, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = DB.DeleteBook(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func ListBook(w http.ResponseWriter, r *http.Request) {
	books, err := DB.ListBook()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(books)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(js)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	var n rep.NewUser
	err := json.Unmarshal([]byte(query), &n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = DB.CreateUser(n.Name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func ReadUser(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	id, err := strconv.ParseUint(query, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user, err := DB.ReadUser(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	js, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(js)
}
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	err := DB.UpdateUser([]byte(query))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	query = query[3:]
	id, err := strconv.ParseUint(query, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = DB.DeleteUser(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func ListUser(w http.ResponseWriter, r *http.Request) {
	users, err := DB.ListUser()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(js)
}
func BookRental(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	var rb rep.RentBook
	json.Unmarshal([]byte(query), &rb)
	err := DB.BookRental(rb.UserID, rb.BookID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func BookReturn(w http.ResponseWriter, r *http.Request) {
	query := read(r.Body)
	var rb rep.RentBook
	json.Unmarshal([]byte(query), &rb)
	err := DB.BookReturn(rb.UserID, rb.BookID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func read(r io.ReadCloser) (query string) {
	var err error
	var n int
	b := make([]byte, 16)
	for err == nil {
		n, err = r.Read(b)
		query = query + string(b[:n])
	}
	return
}
