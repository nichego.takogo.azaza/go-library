module gitlab.com/nichego.takogo.azaza/go-library

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.10
	github.com/jmoiron/sqlx v1.3.5
)

require github.com/lib/pq v1.10.9

require github.com/brianvoe/gofakeit v3.18.0+incompatible
