package repository

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func init() {
	for i := 0; i < 10; i++ {
		time.Sleep(time.Second * 4)
		info := NewDB()
		db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", info.Login, info.Password, info.Path, info.DB))
		if err != nil {
			fmt.Printf("Try %d/10: %s\n", i+1, err)
			continue
		}
		defer db.Close()
		i += 15
		fmt.Println("Connected!")
		_, err = db.Queryx("SELECT id FROM author")
		if err != nil {
			_, err = db.Queryx(`CREATE TABLE "author" (
				"id" serial NOT NULL,
				PRIMARY KEY ("id"),
				"name" character varying(100) NOT NULL,
				"experience" integer NULL,
				"books" json NOT NULL,
				"reading" integer NOT NULL
			  );`)
			if err != nil {
				fmt.Println(err)
			}
		}
		_, err = db.Queryx("SELECT id FROM book")
		if err != nil {
			_, err = db.Queryx(`CREATE TABLE "book" (
				"id" serial NOT NULL,
				PRIMARY KEY ("id"),
				"name" character varying(150) NOT NULL,
				"description" character varying(5000) NOT NULL,
				"released" integer NOT NULL,
				"numberofpages" integer NOT NULL,
				"author" json NOT NULL,
				"rented" boolean NOT NULL,
				"rentinguserid" integer NOT NULL,
				"reading" integer NOT NULL
			  );`)
			if err != nil {
				fmt.Println(err)
			}
		}
		_, err = db.Queryx("SELECT id FROM userr")
		if err != nil {
			_, err = db.Queryx(`CREATE TABLE "userr" (
				"id" serial NOT NULL,
				PRIMARY KEY ("id"),
				"name" character varying(100) NOT NULL,
				"rentedbooks" json NOT NULL,
				"reading" integer NOT NULL
			  );`)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

type Book struct {
	ID            uint   `json:"id"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	Released      uint   `json:"released"`
	NumberOfPages uint   `json:"numberofpages"`
	Author        Author `json:"author"`
	Rented        bool   `json:"rented"`
	RentingUserID uint   `json:"rentinguserid"`
	Reading       uint   `json:"reading"`
}
type Author struct {
	ID         uint   `json:"id"`
	Name       string `json:"name"`
	Experience uint8  `json:"experience"`
	Books      []Book `json:"books"`
	Reading    uint   `json:"reading"`
}
type User struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	RentedBooks []Book `json:"rentedbooks"`
	Reading     uint   `json:"reading"`
}

type NewBook struct {
	Name          string `json:"name"`
	Description   string `json:"description"`
	Released      uint   `json:"released"`
	NumberOfPages uint   `json:"numberofpages"`
	AuthorID      uint   `json:"authorid"`
}
type NewAuthor struct {
	Name       string `json:"name"`
	Experience uint8  `json:"experience"`
}
type NewUser struct {
	Name string `json:"name"`
}
type RentBook struct {
	UserID uint `json:"userid"`
	BookID uint `json:"bookid"`
}

type DB struct {
	Login    string
	Password string
	Path     string
	DB       string
}

func NewDB() DB {
	return DB{
		Login:    "postgres",
		Password: "pass",
		Path:     "post",
		DB:       "postgres",
	}
}

func (v DB) CreateAuthor(name string, experience uint8) error {
	a := Author{
		Name:       name,
		Experience: experience,
		Books:      make([]Book, 0, 10),
	}
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	books, _ := json.Marshal(a.Books)
	db.QueryRowx("INSERT INTO author (name, experience, books, reading) VALUES (" + fmt.Sprintf("'%s', %d, '%s', %d", a.Name, a.Experience, string(books), a.Reading) + ")")
	return nil
}
func (v DB) ReadAuthor(id uint) (Author, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return Author{}, err
	}
	defer db.Close()
	row := db.QueryRowx("SELECT * FROM author WHERE id=" + fmt.Sprint(id))
	if row.Err() != nil {
		return Author{}, row.Err()
	}
	var ans Author
	books := make([]byte, 0, 200)
	err = row.Scan(&ans.ID, &ans.Name, &ans.Experience, &books, &ans.Reading)
	if err != nil {
		return ans, fmt.Errorf(err.Error() + " also maybe wrong author id")
	}
	json.Unmarshal(books, &ans.Books)
	return ans, nil
}
func (v DB) UpdateAuthor(js []byte) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	var author Author
	json.Unmarshal(js, &author)
	reala, err := v.ReadAuthor(author.ID)
	if err != nil {
		return err
	}
	books := reala.Books
	helper := author
	helper.Books = nil
	for i := range author.Books {
		author.Books[i].Author = helper
	}
	js, _ = json.Marshal(author.Books)
	row := db.QueryRowx("UPDATE author SET " + fmt.Sprintf("name='%s', experience=%d, books='%s', reading=%d ", author.Name, author.Experience, string(js), author.Reading) + "WHERE id=" + fmt.Sprint(author.ID))
	if row.Err() != nil {
		return row.Err()
	}
	for i := range books {
		book, err := v.ReadBook(books[i].ID)
		if err != nil {
			continue
		}
		book.Author = helper
		js, _ = json.Marshal(book.Author)
		row := db.QueryRowx("UPDATE book SET " + fmt.Sprintf("name='%s', description='%s', released=%d, numberofpages=%d, author='%s', rented='%t', rentinguserid=%d, reading=%d ", book.Name, book.Description, book.Released, book.NumberOfPages, string(js), book.Rented, book.RentingUserID, book.Reading) + "WHERE id=" + fmt.Sprint(book.ID))
		if row.Err() != nil {
			return row.Err()
		}
	}
	return nil
}
func (v DB) DeleteAuthor(id uint) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	author, err := v.ReadAuthor(id)
	if err != nil {
		return err
	}
	for _, bk := range author.Books {
		book, err := v.ReadBook(bk.ID)
		if err != nil {
			continue
		}
		book.Author = Author{Name: "DELETED AUTHOR"}
		js, _ := json.Marshal(book)
		v.UpdateBook(js)
	}
	db.QueryRowx("DELETE FROM author WHERE id = " + fmt.Sprint(id))
	return nil
}
func (v DB) ListAuthor() ([]Author, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Queryx("SELECT * FROM author")
	if err != nil {
		return nil, err
	}
	ans := make([]Author, 0, 20)
	books := make([]byte, 0, 300)
	for i := 0; rows.Next(); i++ {
		ans = append(ans, Author{})
		rows.Scan(&ans[i].ID, &ans[i].Name, &ans[i].Experience, &books, &ans[i].Reading)
		json.Unmarshal(books, &ans[i].Books)
	}
	return ans, nil
}
func (v DB) Top() ([]Author, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Queryx("SELECT id, name, reading FROM author")
	if err != nil {
		return nil, err
	}
	ans := make([]Author, 0, 20)
	for i := 0; rows.Next(); i++ {
		ans = append(ans, Author{})
		rows.Scan(&ans[i].ID, &ans[i].Name, &ans[i].Reading)
	}
	var id uint
	var value uint
	count := 3
	if len(ans) < 3 {
		count = len(ans)
	}
	ans2 := make([]Author, 0, 3)
	for ; count > 0; count-- {
		id = 0
		value = 0
		for i, v := range ans {
			if v.Reading > value {
				value = v.Reading
				id = uint(i)
			}
		}
		ans2 = append(ans2, ans[id])
		ans = append(ans[:id], ans[id+1:]...)
	}
	ans3 := make([]Author, 0, 3)
	for _, vv := range ans2 {
		author, _ := v.ReadAuthor(vv.ID)
		author.Books = nil
		ans3 = append(ans3, author)
	}
	return ans3, nil
}

func (v DB) CreateBook(nb NewBook) error {
	b := Book{
		Name:          nb.Name,
		Description:   nb.Description,
		Released:      nb.Released,
		NumberOfPages: nb.NumberOfPages,
	}
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	author, err := v.ReadAuthor(nb.AuthorID)
	if err != nil {
		return err
	}
	forbook := author
	forbook.Books = nil
	b.Author = forbook
	js, err := json.Marshal(b.Author)
	if err != nil {
		return err
	}
	row := db.QueryRowx("INSERT INTO book (name, description, released, numberofpages, author, rented, rentinguserid, reading) VALUES (" + fmt.Sprintf("'%s', '%s', %d, %d, '%s', %t, %d, %d", b.Name, b.Description, b.Released, b.NumberOfPages, string(js), b.Rented, b.RentingUserID, b.Reading) + ")")
	if row.Err() != nil {
		return row.Err()
	}
	rows, _ := db.Queryx("SELECT id FROM book WHERE description='" + b.Description + "' AND name='" + b.Name + "'")
	var id uint
	var last uint
	for rows.Next() {
		rows.Scan(&id)
		if last < id {
			last = id
		}
	}
	b.ID = last
	author.Books = append(author.Books, b)
	js, err = json.Marshal(author)
	if err != nil {
		return err
	}
	err = v.UpdateAuthor(js)
	if err != nil {
		return err
	}
	return nil
}
func (v DB) ReadBook(id uint) (Book, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return Book{}, err
	}
	defer db.Close()
	row := db.QueryRowx("SELECT * FROM book WHERE id=" + fmt.Sprint(id))
	if row.Err() != nil {
		return Book{}, row.Err()
	}
	var ans Book
	author := make([]byte, 0, 200)
	err = row.Scan(&ans.ID, &ans.Name, &ans.Description, &ans.Released, &ans.NumberOfPages, &author, &ans.Rented, &ans.RentingUserID, &ans.Reading)
	if err != nil {
		return ans, fmt.Errorf(err.Error() + " also maybe wrong book id")
	}
	json.Unmarshal(author, &ans.Author)
	return ans, nil
}
func (v DB) UpdateBook(js []byte) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	var book Book
	json.Unmarshal(js, &book)
	book.Author.Books = nil
	realb, err := v.ReadBook(book.ID)
	if err != nil {
		return err
	}
	if book.Author.ID != 0 && book.Author.Name != "DELETED AUTHOR" {
		if realb.Author.ID != book.Author.ID {
			author, err := v.ReadAuthor(book.Author.ID)
			if err != nil {
				return err
			}
			author.Books = append(author.Books, book)
			js2, _ := json.Marshal(author)
			v.UpdateAuthor(js2)
			author.Books = nil
			book.Author = author
			author, err = v.ReadAuthor(realb.Author.ID)
			if err == nil {
				for i := range author.Books {
					if author.Books[i].ID == book.ID {
						author.Books = append(author.Books[:i], author.Books[i+1:]...)
						break
					}
				}
				js2, _ := json.Marshal(author)
				v.UpdateAuthor(js2)
			}
		} else {
			author, err := v.ReadAuthor(book.Author.ID)
			if err != nil {
				return err
			}
			for i := range author.Books {
				if author.Books[i].ID == book.ID {
					author.Books = append(author.Books[:i], author.Books[i+1:]...)
					author.Books = append(author.Books, book)
					break
				}
			}
			js2, _ := json.Marshal(author)
			v.UpdateAuthor(js2)
		}
	} else {
		book.Author = Author{Name: "DELETED AUTHOR"}
	}
	js, _ = json.Marshal(book.Author)
	row := db.QueryRowx("UPDATE book SET " + fmt.Sprintf("name='%s', description='%s', released=%d, numberofpages=%d, author='%s', rented='%t', rentinguserid=%d, reading=%d ", book.Name, book.Description, book.Released, book.NumberOfPages, string(js), book.Rented, book.RentingUserID, book.Reading) + "WHERE id=" + fmt.Sprint(book.ID))
	if row.Err() != nil {
		return row.Err()
	}
	return nil
}
func (v DB) DeleteBook(id uint) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	book, err := v.ReadBook(id)
	if err != nil {
		return err
	}
	author, err := v.ReadAuthor(book.Author.ID)
	if err == nil {
		for i := range author.Books {
			if author.Books[i].ID == book.ID {
				author.Books = append(author.Books[:i], author.Books[i+1:]...)
				break
			}
		}
		js, _ := json.Marshal(author)
		v.UpdateAuthor(js)
	}
	db.QueryRowx("DELETE FROM book WHERE id = " + fmt.Sprint(id))
	return nil
}
func (v DB) ListBook() ([]Book, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Queryx("SELECT * FROM book")
	if err != nil {
		return nil, err
	}
	ans := make([]Book, 0, 20)
	author := make([]byte, 0, 300)
	for i := 0; rows.Next(); i++ {
		ans = append(ans, Book{})
		rows.Scan(&ans[i].ID, &ans[i].Name, &ans[i].Description, &ans[i].Released, &ans[i].NumberOfPages, &author, &ans[i].Rented, &ans[i].RentingUserID, &ans[i].Reading)
		json.Unmarshal(author, &ans[i].Author)
	}
	return ans, nil
}

func (v DB) CreateUser(name string) error {
	u := User{
		Name:        name,
		RentedBooks: make([]Book, 0, 10),
	}
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	books, _ := json.Marshal(u.RentedBooks)
	db.QueryRowx("INSERT INTO userr (name, rentedbooks, reading) VALUES (" + fmt.Sprintf("'%s', '%s', %d", u.Name, string(books), u.Reading) + ")")
	return nil
}
func (v DB) ReadUser(id uint) (User, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return User{}, err
	}
	defer db.Close()
	row := db.QueryRowx("SELECT * FROM userr WHERE id=" + fmt.Sprint(id))
	if row.Err() != nil {
		return User{}, row.Err()
	}
	var ans User
	books := make([]byte, 0, 200)
	err = row.Scan(&ans.ID, &ans.Name, &books, &ans.Reading)
	if err != nil {
		return ans, fmt.Errorf(err.Error() + " also maybe wrong user id")
	}
	json.Unmarshal(books, &ans.RentedBooks)
	return ans, nil
}
func (v DB) UpdateUser(js []byte) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	var user User
	json.Unmarshal(js, &user)
	_, err = v.ReadUser(user.ID)
	if err != nil {
		return err
	}
	row := db.QueryRowx("UPDATE userr SET " + fmt.Sprintf("name='%s', reading=%d ", user.Name, user.Reading) + "WHERE id=" + fmt.Sprint(user.ID))
	if row.Err() != nil {
		return row.Err()
	}
	return nil
}
func (v DB) DeleteUser(id uint) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	user, err := v.ReadUser(id)
	if err != nil {
		return err
	}
	for _, bk := range user.RentedBooks {
		v.BookReturn(user.ID, bk.ID)
	}
	db.QueryRowx("DELETE FROM userr WHERE id=" + fmt.Sprint(id))
	return nil
}
func (v DB) ListUser() ([]User, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Queryx("SELECT * FROM userr")
	if err != nil {
		return nil, err
	}
	ans := make([]User, 0, 20)
	books := make([]byte, 0, 300)
	for i := 0; rows.Next(); i++ {
		ans = append(ans, User{})
		rows.Scan(&ans[i].ID, &ans[i].Name, &books, &ans[i].Reading)
		json.Unmarshal(books, &ans[i].RentedBooks)
	}
	return ans, nil
}
func (v DB) BookRental(uid, bid uint) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	user, err := v.ReadUser(uid)
	if err != nil {
		return err
	}
	book, err := v.ReadBook(bid)
	if err != nil {
		return err
	}
	if book.Rented {
		return fmt.Errorf("book already rented")
	}
	user.RentedBooks = append(user.RentedBooks, book)
	book.Rented = true
	book.RentingUserID = user.ID
	book.Reading++
	user.Reading++
	books, _ := json.Marshal(user.RentedBooks)
	row := db.QueryRowx("UPDATE userr SET " + fmt.Sprintf("rentedbooks='%s', reading=%d ", string(books), user.Reading) + "WHERE id=" + fmt.Sprint(user.ID))
	if row.Err() != nil {
		return row.Err()
	}
	js, _ := json.Marshal(book)
	v.UpdateBook(js)
	author, err := v.ReadAuthor(book.Author.ID)
	if err != nil {
		return err
	}
	author.Reading++
	js, _ = json.Marshal(author)
	v.UpdateAuthor(js)
	return nil
}
func (v DB) BookReturn(uid, bid uint) error {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	user, err := v.ReadUser(uid)
	if err != nil {
		return err
	}
	book, err := v.ReadBook(bid)
	if err != nil {
		return err
	}
	var finded bool
	for i := range user.RentedBooks {
		if user.RentedBooks[i].ID == bid {
			user.RentedBooks = append(user.RentedBooks[:i], user.RentedBooks[i+1:]...)
			finded = true
		}
	}
	if finded {
		book.Rented = false
		book.RentingUserID = 0
		books, _ := json.Marshal(user.RentedBooks)
		row := db.QueryRowx("UPDATE userr SET " + fmt.Sprintf("rentedbooks='%s'", string(books)) + "WHERE id=" + fmt.Sprint(user.ID))
		if row.Err() != nil {
			return row.Err()
		}
		js, _ := json.Marshal(book)
		v.UpdateBook(js)
	} else {
		return fmt.Errorf("this user has not rented this book")
	}

	return nil
}
