package fakeit

import (
	"fmt"

	fake "github.com/brianvoe/gofakeit"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	rep "gitlab.com/nichego.takogo.azaza/go-library/repository"
)

func IfDBEmpty() error {
	v := rep.NewDB()
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", v.Login, v.Password, v.Path, v.DB))
	if err != nil {
		return err
	}
	defer db.Close()
	rows, err := db.Queryx("SELECT id FROM author")
	if err != nil {
		return err
	}
	var a int
	for rows.Next() {
		a++
	}
	rows, err = db.Queryx("SELECT id FROM book")
	if err != nil {
		return err
	}
	var b int
	for rows.Next() {
		b++
	}
	rows, err = db.Queryx("SELECT id FROM userr")
	if err != nil {
		return err
	}
	var u int
	for rows.Next() {
		u++
	}
	if a <= 10 && b <= 20 && u <= 15 {
		a = fake.Number(10, 14)
		for i := 0; i < a; i++ {
			v.CreateAuthor(fake.Name(), uint8(fake.Number(0, 12)))
		}
		b = fake.Number(100, 125)
		for i := 0; i < b; i++ {
			r := fake.Number(1, 5)
			nb := rep.NewBook{
				Name:          fake.Sentence(r),
				Description:   fake.Paragraph(1, 3, 14, ""),
				Released:      uint(fake.Year()),
				NumberOfPages: uint(fake.Number(30, 3000)),
				AuthorID:      uint(fake.Number(1, a)),
			}
			v.CreateBook(nb)
		}
		u = fake.Number(50, 70)
		for i := 0; i < u; i++ {
			v.CreateUser(fake.Username())
		}
	}
	return nil
}
